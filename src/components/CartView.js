import { useContext, useEffect, useState } from 'react';
import { Container, Table } from 'react-bootstrap';
import UserContext from '../UserContext';

export default function CartView() {
  const { user } = useContext(UserContext);
  const [orderedProducts, setOrderedProducts] = useState([]);

  useEffect(() => {
    if (user.id) {
      fetch(`http://localhost:4005/users/${user.id}/orders`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      })
      .then(response => response.json())
      .then(data => {
        setOrderedProducts(data);
      })
      .catch(error => console.error(error));
    }
  }, [user.id])

  return (
    <Container>
      <h2>My Orders</h2>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Order ID</th>
            <th>Product Name</th>
            <th>Quantity</th>
            <th>Date Ordered</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {orderedProducts.length > 0 ? (
            orderedProducts.map(order => (
              <tr key={order.id}>
                <td>{order.id}</td>
                <td>{order.product_name}</td>
                <td>{order.quantity}</td>
                <td>{order.date_ordered}</td>
                <td>{order.price}</td>
              </tr>
            ))
          ) : (
            <tr>
              <td colSpan="5">No orders found.</td>
            </tr>
          )}
        </tbody>
      </Table>
    </Container>
  )
}
